#include <iostream>
#include <algorithm>
#include <random>
#include <chrono>

#include "Kokkos_Core.hpp"

namespace kk = Kokkos;

typedef Kokkos::TeamPolicy<>               team_policy;
typedef Kokkos::TeamPolicy<>::member_type  member_type;

const int varDim = 1;
const int spaceDim = 2;

using Vector = kk::Array<double, varDim>;
using VecVec = kk::Array<Vector, spaceDim>;

class Flux
{
public:
  KOKKOS_INLINE_FUNCTION VecVec get_flux(Vector data) const {
    return VecVec{Vector{ax_*data[0]}, Vector{ay_*data[0]}};
  }

  double ax_, ay_;
  
};

int main(int argc, char** argv) {
  kk::initialize();

  printf("ndg2d test on Kokkos execution space %s\n",
         typeid(Kokkos::DefaultExecutionSpace).name());

  if(argc < 3) {
    std::cerr << "ERROR! Usage: ./main n nIter (n > 128)\n";
    exit(1);
  }

  int nx = atoi(argv[1]);
  int nIter = atoi(argv[2]);

  std::cout << "n = " << nx << ", nIter = " << nIter << std::endl;
  
  {
    const int order = 6;
    const int varDim = 1;
    //allocated on device:
    // kk::View<double**[order][order][varDim], kk::LayoutLeft>  a_v("a", nx, nx);
    // kk::View<double**[order][order][2][varDim], kk::LayoutLeft>  flux_v("a", nx, nx);
    kk::View<double**[order][order][varDim], kk::LayoutRight>  a_v("a", nx, nx);
    kk::View<double**[order][order][2][varDim], kk::LayoutRight>  flux_v("a", nx, nx);

    // // host view (on cpu) with size of view b
    // kk::View<double**[order][order][varDim]>::HostMirror ah = kk::create_mirror(a_v);

    // // initialize ah on cpu (not on GPU)
    // for(int i=0;i<nx;i++) 
    //   for(int j=0;j<nx;j++) 
    // 	for(int k=0;k<order;k++)
    // 	  for(int l=0;l<order;l++)
    // 	    for(int m=0;m<varDim;m++) 
    // 	      ah(i,j,k,l,m) = (i+1)*10000 + (j+1)*1000 + (k+1)*100 + (l+1)*10 + m+1;
    

    // Kokkos::deep_copy(a_v, ah);
      
    // kk::parallel_for("plus", kk::MDRangePolicy<kk::Rank<3>>({0,0,0},{nx,nx,nx}),
    //                  KOKKOS_LAMBDA(int i, int j, int k) {
    // 		       a(i,j,k) += 1;
    //                  });

    
    Flux flux;

    auto start = std::chrono::system_clock::now();

    for(int iter = 0; iter<nIter; iter++) {
      //      auto range = kk::MDRangePolicy<kk::Rank<4,kk::Iterate::Left, kk::Iterate::Left>>({0,0,0,0},{a_v.extent(0), a_v.extent(1), a_v.extent(2), a_v.extent(3)});
      auto range = kk::MDRangePolicy<kk::Rank<4,kk::Iterate::Right, kk::Iterate::Right>>({0,0,0,0},{a_v.extent(0), a_v.extent(1), a_v.extent(2), a_v.extent(3)});
      Kokkos::parallel_for("plus", range, KOKKOS_LAMBDA (int cellX, int cellY, int ix, int iy) {
	  Vector data;
	  for(int var=0; var<varDim; var++)
	    data[var] = a_v(cellX, cellY, ix, iy, var);
	  
	  VecVec fluxArray = flux.get_flux(data);

	  for(int var=0; var<varDim; var++) {
	    flux_v(cellX, cellY, ix, iy, 0, var) = fluxArray[0][var];
	    flux_v(cellX, cellY, ix, iy, 1, var) = fluxArray[1][var];
	  }
	});
    }

    auto end = std::chrono::system_clock::now();
    
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    
    std::cout << "finished computation at " << std::ctime(&end_time)
	      << "elapsed time: " << elapsed_seconds.count() << "s\n";
    
    // Kokkos::parallel_for("plus", team_policy(nx, Kokkos::AUTO), KOKKOS_LAMBDA ( const member_type &teamMember) {
    // 	const int k = teamMember.league_rank();
	
    // 	Kokkos::parallel_for( Kokkos::TeamThreadRange( teamMember, nx ), [&] (const int j) {
    // 	  Kokkos::parallel_for( Kokkos::ThreadVectorRange( teamMember, nx ), [&] ( const int i) {
    // 	    a(i,j,k) += 1;
    // 	  });
    // 	});
    //   });
    
    //    Kokkos::deep_copy(ah, a_v);
    
    // for(int i=0;i<nx;i++) 
    //   for(int j=0;j<nx;j++) 
    // 	for(int k=0;k<nx;k++) 
    // 	  std::cout << ah(i,j,k) << "\t";
    
    // std::cout << std::endl;
  }

  kk::finalize();
  std::cout << "terminating main\n";
  return 0;
}
