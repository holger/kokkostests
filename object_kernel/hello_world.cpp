#include <iostream>
#include <algorithm>
#include <random>
#include <array>

#include "Kokkos_Core.hpp"

namespace kk = Kokkos;

// possible to replace by custum class Arr?
template<typename T, int num>
class Array {
public:
  KOKKOS_INLINE_FUNCTION T operator[](int i) const {
    return arr_[i];
  }
  KOKKOS_INLINE_FUNCTION T& operator[](int i) {
    return arr_[i];
  }
private:
  T arr_[num];
};

template<int num>
class Flux
{
public:
  Flux(double a) : a_(a) {}

  KOKKOS_INLINE_FUNCTION Array<double, num> get_flux(Array<double, num> const& arr) const {
    Array<double, num> tmp;
    tmp[0] = arr[0]*a_;
    tmp[1] = arr[1]*a_;
    tmp[2] = arr[2]*a_;
    return tmp;
  }
  
  double a_;
};

int main(int argc, char** argv) {
  kk::initialize();

  printf("Hello World on Kokkos execution space %s\n",
         typeid(Kokkos::DefaultExecutionSpace).name());

  {
    int const nx = 10;
    //allocated on device:
    kk::View<float*>  a("a", nx);
    kk::View<float*>  b("b", nx);

    // host view (on cpu) with size of view b
    kk::View<float*>::HostMirror bh = kk::create_mirror(b);

    // initialize bh on cpu (not on GPU)
    for(int i=0;i<nx;i++) {
      bh(i) = i;
    }

    Kokkos::deep_copy(b, bh);

    const int num = 3;
    Flux<num> flux(2.1);
    Array<double, num> arr;
    arr[0] = 1;
    arr[1] = 2;
    arr[2] = 3;
    
    kk::parallel_for("init", nx,
                     KOKKOS_LAMBDA(int i) {
		       a(i) = 0;
		       for(int k=0; k<4; k++)
			 a(i) += flux.get_flux(arr)[1];
                     });

    Kokkos::deep_copy(bh, b);

    kk::View<float*>::HostMirror ah = kk::create_mirror(a);
    Kokkos::deep_copy(ah, a);
    
    for(int i=0;i<nx;i++)
      std::cout << i << "\t" << ah(i) << std::endl;
	
    float sum = 0;
    // .. which is really kk::RangePolicy<DefaultExecutionSpace>(0, nx),
    kk::parallel_reduce("avg", kk::RangePolicy<kk::DefaultExecutionSpace>(0, nx),
                        KOKKOS_LAMBDA(int i, float& partial) { partial += a(i); },
                        sum);
    sum /= nx;
    std::cout << "Average sum value : " << sum << '\n';
  }
  kk::finalize();
  return 0;
}
