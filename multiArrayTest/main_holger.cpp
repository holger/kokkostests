#include <iostream>
#include <algorithm>
#include <random>
#include "Kokkos_Core.hpp"
#include "problem1d_holger.hpp"

// #include "BasisKlassen/trunk/multiArray.hpp"

//const int varDim = 2;

namespace kk = Kokkos;

int main(int argc, char** argv) {
  kk::initialize();
  {

  printf("Running in execution space %s, HostSpace %s\n", 
        typeid(Kokkos::DefaultExecutionSpace).name(), typeid(Kokkos::DefaultHostExecutionSpace).name());

  int nCell = 10;
  const int order = 5;

  kk::View<double**[varDim]> array("arr", nCell, order);

  auto host_array = kk::create_mirror_view(array);

  for(int cell=0; cell < array.extent(0); cell++)
    for(int index=0; index < array.extent(1); index++)
      for(int var=0; var < array.extent(2); var++)
      {
        host_array(cell, index, var) = 2.5;
        std::cout << host_array(cell, index, var) << std::endl;
      }

  kk::deep_copy(array, host_array);

  kk::View<Flux*> f("f", 1);
  Problem1d::Addition add{array,f};

  Problem1d::Subtraction minus{array};

  kk::parallel_for("Initialisation", add.getPolicy(), add);

  kk::parallel_for("Initialisation", minus.getPolicy(), minus);

  kk::deep_copy(host_array, array);

  for(int cell=0; cell < array.extent(0); cell++)
    for(int index=0; index < array.extent(1); index++)
      for(int var=0; var < array.extent(2); var++)
	std::cout << host_array(cell, index, var) << std::endl;
  }
  kk::finalize();
  return 0;
}
