#include <iostream>
#include <algorithm>
#include <random>
#include "Kokkos_Core.hpp"
#include "problem1d.hpp"


// #include "BasisKlassen/trunk/multiArray.hpp"

//const int varDim = 2;

namespace kk = Kokkos;
int main(int argc, char** argv) {
  kk::initialize();
  {
  // printf("Hello World on Kokkos execution space %s\n",
  //        typeid(Kokkos::DefaultExecutionSpace).name());

  printf("Running in execution space %s, HostSpace %s\n", 
        typeid(Kokkos::DefaultExecutionSpace).name(), typeid(Kokkos::DefaultHostExecutionSpace).name());

  int nCell = 10;
  const int order = 5;
  // BK::MultiArray<2,BK::Array<double,varDim>> array(nCell,order);

  // for(int cell=0; cell < array.size(0); cell++)
  //   for(int index=0; index < array.size(1); index++)
  //     for(int var=0; var < varDim; var++)
	// array(cell,index)[var] = 0.;

  kk::View<double**[varDim]> array("arr", nCell, order);

  kk::Profiling::pushRegion("Setup");
  auto host_array = kk::create_mirror_view(array);

  for(int cell=0; cell < array.extent(0); cell++)
    for(int index=0; index < array.extent(1); index++)
      for(int var=0; var < array.extent(2); var++)
      {
        host_array(cell, index, var) = 2.5;
        std::cout << host_array(cell, index, var) << std::endl;
      }

  kk::deep_copy(array, host_array);

  // initialisation. init_func can be pulled from a header file and be defined according to the array dimensions

  //  DEVICE_VARIABLE static InitFunc init_func;
  DEVICE_VARIABLE InitFunctor init_functor;

  Problem1d::Initialise initialise{array, init_functor}; //use array or host array. Host array allows for file reading, device array is faster.`

  //trying to pass a function to  be used in the initialisiation but get illegal memory access on GPU despite creating device variable

  std::cout << "Init parallel for" << std::endl;

  kk::parallel_for("Init", initialise.getPolicy(), initialise); //illegal memory access here if on GPU

  kk::deep_copy(host_array, array);

  for(int cell=0; cell < array.extent(0); cell++)
    for(int index=0; index < array.extent(1); index++)
      for(int var=0; var < array.extent(2); var++)
	std::cout << host_array(cell, index, var) << std::endl;

  // kk::parallel_for("Initialisation", policy, KOKKOS_LAMBDA(int cell, int index, int var) {
  //   array(cell, index, var) += 1;
  //   //there is probably a better way of doing this btw using kokkos initialise
  // });

  //Addition addition{array};

  //kk::View<Flux*> f("f", 1);
  //Problem1d::Addition add{array,f};
  Problem1d::Addition add{array};
  Problem1d::Subtraction minus{array};

  kk::parallel_for("Initialisation", add.getPolicy(), add);

  kk::parallel_for("Initialisation", minus.getPolicy(), minus);

  kk::Profiling::popRegion();
  //kk::paralle_for("SomeCalc", policy, problem1d.fluxcalc(array))

  kk::deep_copy(host_array, array);

  for(int cell=0; cell < array.extent(0); cell++)
    for(int index=0; index < array.extent(1); index++)
      for(int var=0; var < array.extent(2); var++)
	std::cout << host_array(cell, index, var) << std::endl;


  
  // {
  //   int const nx = 10;
  //   //allocated on device:
  //   kk::View<float*>  a("a", nx);
  //   kk::View<float*>  b("b", nx);
  //   kk::View<float*>  c("c", nx);

  //   // host view (on cpu) with size of view b
  //   kk::View<float*>::HostMirror bh = kk::create_mirror(b);

  //   // let use auto instead
  //   auto ch = kk::create_mirror(c);

  //   // initialize bh on cpu (not on GPU)
  //   for(int i=0;i<nx;i++) {
  //     bh(i) = i;
  //     ch(i) = 2*i;
  //   }

  //   Kokkos::deep_copy(b, bh);
  //   Kokkos::deep_copy(c, ch);
      
  //   kk::parallel_for("init", nx,
  //                    KOKKOS_LAMBDA(int i) {
  // 		       a(i) = b(i) + c(i);
  //                    });

  //   Kokkos::deep_copy(bh, b);
  //   Kokkos::deep_copy(ch, c);
    
  //   // for(int i=0;i<nx;i++)
  //   //   std::cout << i << "\t" << bh(i) << "\t" << ch(i) << std::endl;
    
  //   // .. kk::RangePolicy<>(0, nx), which is a shortcut for 
  //   //    kk::parallel_for("sum", kk::RangePolicy<>(0, nx), KOKKOS_LAMBDA(int i) { a(i) = b(i)+c(i); });

  //   kk::View<float*>::HostMirror ah = kk::create_mirror(a);
  //   Kokkos::deep_copy(ah, a);
    
  //   for(int i=0;i<nx;i++)
  //     std::cout << i << "\t" << bh(i) << "\t" << ch(i) << "\t" << ah(i) << std::endl;
	
  //   float sum = 0;
  //   // .. which is really kk::RangePolicy<DefaultExecutionSpace>(0, nx),
  //   kk::parallel_reduce("avg", kk::RangePolicy<kk::DefaultExecutionSpace>(0, nx),
  //                       KOKKOS_LAMBDA(int i, float& partial) { partial += a(i); },
  //                       sum);
  //   sum /= nx;
  //   std::cout << "Average sum value : " << sum << '\n';
  // }
  }
  kk::finalize();
  return 0;
}
