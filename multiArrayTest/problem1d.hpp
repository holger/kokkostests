#include <iostream>
#include <algorithm>
#include <random>
#include "Kokkos_Core.hpp"
// #if defined(KOKKOS_ENABLE_DEFAULT_DEVICE_TYPE_CUDA)
// #define DEVICE_VARIABLE __device__
// #else
// #define DEVICE_VARIABLE
// #endif

#define DEVICE_VARIABLE __attribute__((device))

// #include "BasisKlassen/trunk/multiArray.hpp"

const int varDim = 2;

namespace kk = Kokkos;

class Flux
{
    public:
        KOKKOS_INLINE_FUNCTION
        double flux(double a)
        {
            return a*2 + b;
        };

  double b = 10;
};

class InitFunc //make another header file with this instead?
{
  public:
    KOKKOS_INLINE_FUNCTION
    static double zero(double const& array_value)
    {
      return 0;
    };

    KOKKOS_INLINE_FUNCTION
    double one(double const& array_value)
    {
      return 1;
    };
};

class InitFunctor //make another header file with this instead?
{
  public:
    KOKKOS_INLINE_FUNCTION
    double operator()(double const& array_value) const
    {
      return 2.5;
    };

    // KOKKOS_INLINE_FUNCTION
    // double one(double const& array_value)
    // {
    //   return 1;
    // };
};

DEVICE_VARIABLE Flux f;
class Problem1d
{
  public:

    struct Addition
    {
      kk::View<double**[varDim]> array;
      //kk::View<Flux*> f;
      
      auto getPolicy(){return kk::MDRangePolicy<kk::Rank<3>>({0, 0, 0},{array.extent(0), array.extent(1), array.extent(2)});};
      
      KOKKOS_INLINE_FUNCTION void operator()(int cell, int index, int var) const { //maybe change the ints to unsigned ints
	      array(cell, index, var) += f.flux(cell);
      }
    }; 

    struct Subtraction
    {
    kk::View<double**[varDim]> array;
    auto getPolicy(){return kk::MDRangePolicy<kk::Rank<3>>({0, 0, 0},{array.extent(0), array.extent(1), array.extent(2)});};

    KOKKOS_INLINE_FUNCTION void operator()(int cell, int index, int var) const { //we don't actually need to overload the operator because kk:View(i, j, k) is how you access arrays
      array(cell, index, var) -= 1;
    }
    };

    struct Initialise
    {
      kk::View<double**[varDim]> array; //this needs to be dynamic, maybe only use compile time args? OR Define 1d, 2d and 3d separately might be better.
      //      DEVICE_VARIABLE double (*init_func)(double const&); //this needs to be on the device
      DEVICE_VARIABLE InitFunctor init_func; //this needs to be on the device

      auto getPolicy(){return kk::MDRangePolicy<kk::Rank<3>>({0, 0, 0},{array.extent(0), array.extent(1), array.extent(2)});};

      // KOKKOS_INLINE_FUNCTION void operator()(int cell, int index, int var) const { 
      //   array(cell, index, var) = 0; //redundant because kokkos arrays initilise at 0 
      // };

      KOKKOS_INLINE_FUNCTION void operator()(int cell, int index, int var) const { 
        array(cell, index, var) = 1; //if uncommented, this works
        //array(cell, index, var) = init_func(array(cell, index, var)); //init_func results in illegal device memory access
      };

    };
};

// struct Addition
// {
//   kk::View<double**[varDim]> array;

//   KOKKOS_INLINE_FUNCTION void operator()(int cell, int index, int var) const {
//     array(cell, index, var) += 1;
//   }
// };
