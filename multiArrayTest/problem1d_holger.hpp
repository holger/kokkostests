#include <iostream>
#include <algorithm>
#include <random>
#include "Kokkos_Core.hpp"
#if defined(KOKKOS_ENABLE_DEFAULT_DEVICE_TYPE_CUDA)
#define DEVICE_VARIABLE __device__
#else
#define DEVICE_VARIABLE
#endif

// #include "BasisKlassen/trunk/multiArray.hpp"

const int varDim = 2;

namespace kk = Kokkos;

class Flux
{
    public:
        KOKKOS_INLINE_FUNCTION
        double flux(double a)
        {
            return a*2 + b;
        };

  double b = 10;
};

//DEVICE_VARIABLE Flux f;
class Problem1d
{
  public:

    struct Addition
    {
      kk::View<double**[varDim]> array;
      kk::View<Flux[1]> f_v;
      
      auto getPolicy(){return kk::MDRangePolicy<kk::Rank<3>>({0, 0, 0},{array.extent(0), array.extent(1), array.extent(2)});};
      
      KOKKOS_INLINE_FUNCTION void operator()(int cell, int index, int var) const {
	Flux& f = f_v(0);
	array(cell, index, var) += f.flux(cell);
      }
    }; 

    struct Subtraction
    {
    kk::View<double**[varDim]> array;
    auto getPolicy(){return kk::MDRangePolicy<kk::Rank<3>>({0, 0, 0},{array.extent(0), array.extent(1), array.extent(2)});};

    KOKKOS_INLINE_FUNCTION void operator()(int cell, int index, int var) const {
      array(cell, index, var) -= 1;
    }
    };     
};

// struct Addition
// {
//   kk::View<double**[varDim]> array;

//   KOKKOS_INLINE_FUNCTION void operator()(int cell, int index, int var) const {
//     array(cell, index, var) += 1;
//   }
// };
