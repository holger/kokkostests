#include <iostream>
#include <algorithm>
#include <random>
#include <chrono>
#include <fstream>
#include <string>

#include "toString.hpp"

#include "Kokkos_Core.hpp"

namespace kk = Kokkos;

template<class TYPE>
KOKKOS_INLINE_FUNCTION double intPow(TYPE x, int potenz)
{
  double tmp = 1;

  for(int i=0;i<potenz;i++){
      tmp *= x;
    }
  return tmp;
}

int main(int argc, char** argv) {

  if(argc < 4) {
    std::cerr << "ERROR! Usage: ./main n nIter output (n > 128)\n";
    exit(1);
  }
  
  int n_ = atoi(argv[1]);
  int nIter_ = atoi(argv[2]);
  int output = atoi(argv[3]);
  
  std::cout << "n = " << n_ << ", nIter = " << nIter_ << std::endl;

  kk::initialize();

  printf("Hello World on Kokkos execution space %s\n",
         typeid(Kokkos::DefaultExecutionSpace).name());

  for(int nIter=1; nIter<=nIter_; nIter*=10)
    for(int n=128; n<=n_; n*=2) {

      std::cout << "n = " << n << ", nIter = " << nIter << std::endl;
      
    //allocated on device:
    kk::View<double*>  ai("a", n);
    kk::View<double*>  a("a", n);

    // host view (on cpu) with size of view b
    auto aih = kk::create_mirror(ai);
    auto ah = kk::create_mirror(a);

    double dx = 1./n;
    // initialize bh on cpu (not on GPU)
    for(int i=0;i<n;i++) {
      aih(i) = i*dx;
    }

    Kokkos::deep_copy(a, aih);
    Kokkos::deep_copy(ai, aih);

    auto start = std::chrono::system_clock::now();
    
    kk::parallel_for("init", n,
                     KOKKOS_LAMBDA(int i) {
		       for(int j = 2; j<nIter; j++)
			 a(i) = a(i) + intPow(ai(i),j);
			 //a(i) = a(i) + kk::pow(ai(i),j);
                     });

    std::cout << "finished\n";
    
    auto end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end-start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    
    std::cout << "finished computation at " << std::ctime(&end_time)
	      << "elapsed time: " << elapsed_seconds.count() << "s\n";

    std::ofstream out(BK::toString("chrono-",typeid(Kokkos::DefaultExecutionSpace).name(),".txt"), std::ios::app);
    out << n << "\t" << nIter << "\t" << elapsed_seconds.count() << std::endl;
    out.close();

    if(output > 0) {
      Kokkos::deep_copy(ah, a);
      
      for(int i=0;i<n;i++)
	std::cout << i << "\t" << aih(i) << "\t" << ah(i) << std::endl;
    }
    
  }
  kk::finalize();
  return 0;
}
