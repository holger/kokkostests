#include <iostream>
#include <algorithm>
#include <cstdlib>

int
main() {
  int const nx = 2000;
  float a[nx];
  float b[nx];
  float c[nx];
  
  std::generate(b, b+nx, [&]() { return -float(std::rand())/RAND_MAX; });
  std::generate(c, c+nx, [&]() { return  float(std::rand())/RAND_MAX; });
  
#pragma omp parallel for
  for (int i = 0; i < nx; ++i) {
    a[i] = b[i]+c[i];
  }
  
  float sum = 0;
#pragma omp parallel for reduction(+:sum)
  for (int i = 0; i < nx; ++i) {
    sum += a[i];
  }
  sum /= nx;
  std::cout << "Average sum value : " << sum << '\n';
  return 0;
}
