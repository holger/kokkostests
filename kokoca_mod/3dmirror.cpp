#include <iostream>
#include <algorithm>
#include <random>
#include "Kokkos_Core.hpp"

namespace kk = Kokkos;

auto hostRange(int n) {
  return kk::RangePolicy<Kokkos::DefaultHostExecutionSpace>(0, n);
}

int
main() {
  kk::initialize();
  {
    int const n1 = 3000;
    int const n2 = 200;
    int const n3 = 10;
    // allocated on device:
    kk::View<float***>  a("a", n1, n2, n3);
    kk::View<float***>  b("b", n1, n2, n3);
    kk::View<float***>  c("c", n1, n2, n3);

    auto bh = kk::create_mirror(b);
    auto ch = kk::create_mirror(c);
    // fill data on host:
    kk::parallel_for("init", kk::MDRangePolicy<kk::Rank<3>, kk::DefaultHostExecutionSpace>({0,0,0}, {n1,n2,3}),
                     KOKKOS_LAMBDA(int i, int j, int k) {
                       bh(i,j,k) = -float(std::rand())/RAND_MAX;
                       ch(i,j,k) =  float(std::rand())/RAND_MAX;
                     });
    // from host to device:
    kk::deep_copy(b, bh);
    kk::deep_copy(c, ch);
    kk::parallel_for("sum", kk::MDRangePolicy<kk::Rank<3>>({0,0,0}, {n1,n2,3}),
                     KOKKOS_LAMBDA(int i, int j, int k) {
                       a(i,j,k) = b(i,j,k)+c(i,j,k);
                     });

    float sum = 0;
    kk::parallel_reduce("avg", kk::MDRangePolicy<kk::Rank<3>>({0,0,0}, {n1,n2,3}),
                        KOKKOS_LAMBDA(int i, int j, int k, float& partial) {
                          partial += a(i,j,k);
                        },
                        sum);
    sum /= a.size();
    std::cout << "Average sum value : " << sum << '\n';
  }
  kk::finalize();
  return 0;
}
