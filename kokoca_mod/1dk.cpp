#include <iostream>
#include <algorithm>
#include <random>
#include "Kokkos_Core.hpp"

namespace kk = Kokkos;

int
main() {
  kk::initialize();
  {
    int const nx = 2000;
    kk::View<float*>  a("a", nx);
    kk::View<float*>  b("b", nx);
    kk::View<float*>  c("c", nx);
    
    std::mt19937 gen;
    std::uniform_real_distribution<float> dist(0, 1);
    for(int i = 0; i < nx; ++i) {
      b(i) = -float(std::rand())/RAND_MAX;
      c(i) =  float(std::rand())/RAND_MAX;
    }
    
    for (int i = 0; i < nx; ++i) {
      a[i] = b[i]+c[i];
    };
    float sum = 0;
    for (int i = 0; i < nx; ++i) {
      sum += a(i);
    }
    sum /= nx;
    std::cout << "Average sum value : " << sum << '\n';
  }
  kk::finalize();
  return 0;
}
