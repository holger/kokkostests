#include <iostream>
#include <algorithm>
#include <random>
#include "Kokkos_Core.hpp"

namespace kk = Kokkos;

auto hostRange(int n) {
  return kk::RangePolicy<Kokkos::DefaultHostExecutionSpace>(0, n);
}

int
main() {
  kk::initialize();
  {
    int const nx = 2000;
    // allocated on device:
    kk::View<float*>  a("a", nx);
    kk::View<float*>  b("b", nx);
    kk::View<float*>  c("c", nx);

    // cumbersome:
    kk::View<float*>::HostMirror bh = kk::create_mirror(b);
    // let use auto instead
    auto ch = kk::create_mirror(c);
    // fill data on host:
    kk::parallel_for("init", kk::RangePolicy<Kokkos::DefaultHostExecutionSpace>(0,nx),
                     KOKKOS_LAMBDA(int i) {
                       bh(i) = -float(std::rand())/RAND_MAX;
                       ch(i) =  float(std::rand())/RAND_MAX;
                     });
    // from host to device:
    kk::deep_copy(b, bh);
    kk::deep_copy(c, ch);
    kk::parallel_for("sum", nx, KOKKOS_LAMBDA(int i) { a(i) = b(i)+c(i); });

    float sum = 0;
    kk::parallel_reduce("avg", nx,
                        KOKKOS_LAMBDA(int i, float& partial) { partial += a(i); },
                        sum);
    sum /= nx;
    std::cout << "Average sum value : " << sum << '\n';
  }
  kk::finalize();
  return 0;
}
