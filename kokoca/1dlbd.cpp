#include <iostream>
#include <algorithm>
#include <random>
#include "Kokkos_Core.hpp"

namespace kk = Kokkos;

int
main() {
  kk::initialize();
  {
    int const nx = 2000;
    // allocated on device:
    kk::View<float*>  a("a", nx);
    kk::View<float*>  b("b", nx);
    kk::View<float*>  c("c", nx);

    // cumbersome:
    kk::View<float*>::HostMirror bh = kk::create_mirror(b);
    // let use auto instead
    auto ch = kk::create_mirror(c);
    // nx is a shortcut, it will be replaced with ...
    kk::parallel_for("init", nx,
                     KOKKOS_LAMBDA(int i) {
                       bh(i) = -float(std::rand())/RAND_MAX;
                       ch(i) =  float(std::rand())/RAND_MAX;
                     });
    // .. kk::RangePolicy<>(0, nx), which is a shortcut for 
    kk::parallel_for("sum", kk::RangePolicy<>(0, nx), KOKKOS_LAMBDA(int i) { a(i) = b(i)+c(i); });

    float sum = 0;
    // .. which is really kk::RangePolicy<DefaultExecutionSpace>(0, nx),
    kk::parallel_reduce("avg", kk::RangePolicy<kk::DefaultExecutionSpace>(0, nx),
                        KOKKOS_LAMBDA(int i, float& partial) { partial += a(i); },
                        sum);
    sum /= nx;
    std::cout << "Average sum value : " << sum << '\n';
  }
  kk::finalize();
  return 0;
}
