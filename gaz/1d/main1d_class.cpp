#include <iostream>
#include <cmath>
#include <fstream>

#include "../deps/BasisKlassen/trunk/array.hpp"
#include "../deps/BasisKlassen/trunk/vector.hpp"
#include "../deps/BasisKlassen/trunk/toString.hpp"

typedef BK::Vector<BK::Array<double,2>> Vector;

template<class Vector>
inline Vector laxFriedrichs1d_flux_numeric(Vector const& fluxM, Vector const& fluxP, double dFluxM, double dFluxP, Vector const& valueM, Vector const& valueP)
{
  Vector f = 0.5*(fluxM + fluxP);

  double c = std::max(fabs(dFluxM), fabs(dFluxP));
  
  return f - 0.5*c*(valueP - valueM);
}

double dFlux(BK::Array<double,2> const& q, double a) {
  double rho = q[0];
  double rhoU = q[1];
  double u = rhoU/rho;
  
  return std::max(fabs(u+a), fabs(u-a));
}

void boundaryCondition(Vector& q)
{
  size_t nCell = q.size();
  q[0] = q[nCell-2];
  q[nCell-1] = q[1];
}

class Gaz
{
public:

  Gaz(double a) : a_(a) {}

  BK::Array<double,2> flux(BK::Array<double,2> const& q) {
    return {q[1], q[1]*q[1]/q[0] + a_*a_*q[0]};
  }

  double dFlux(BK::Array<double,2> const& q) {
    double rho = q[0];
    double rhoU = q[1];
    double u = rhoU/rho;
    
    return std::max(fabs(u+a_), fabs(u-a_));
  }

  double a_;
};

int main()
{
  std::cerr << "Simulation of a isothermal gaz starts ...\n";

  enum Var {rho, mom};

  size_t nCell = 40;
  
  Vector q0(nCell);
  Vector q(nCell);
  Vector qp(nCell-1);
  Vector qm(nCell-1);
  BK::Vector<double> dfm(nCell-1);
  BK::Vector<double> dfp(nCell-1);
  Vector fm(nCell-1);
  Vector fp(nCell-1);
  Vector fi(nCell);
  Vector fo(nCell);

  double lambda = 2*M_PI;
  double dx = lambda/(nCell - 2);
  double lx = lambda + dx;
  int nt = 101;
  double cfl = 0.1;
  double dt = cfl*dx;

  Gaz gaz(0.5);
  
  std::cerr << "dt = " << dt << std::endl;

  for(int i=0; i<nCell; i++) {
    q0[i][rho] = 1;
    q0[i][mom] = sin(2*M_PI/lambda * (i+0.5)*dx);
  }

  std::ofstream out;

  out.open("q0.data");
  for(int i=0; i<nCell; i++)
    out << i*dx+dx/2 << "\t" << q0[i][0] << "\t" << q0[i][1] << std::endl;

  out.close();

  for(int t=0; t<nt;t++) {

    if(t%10 == 0) {
      out.open(BK::toString("q-",t,".data"));
      for(int i=0; i<nCell; i++)
	out << i*dx+dx/2 << "\t" << q0[i][0] << "\t" << q0[i][1] << std::endl;
      
      out.close();
    }
    
    for(int i=0; i<nCell-1; i++) {

      qm[i][0] = q0[i][0];
      qm[i][1] = q0[i][1];
      qp[i][0] = q0[i+1][0];
      qp[i][1] = q0[i+1][1];

      dfm[i] = gaz.dFlux(q0[i]);
      dfp[i] = gaz.dFlux(q0[i+1]);

      auto flux = gaz.flux(q0[i]);
      fm[i][0] = flux[0];
      fm[i][1] = flux[1];

      flux = gaz.flux(q0[i+1]);
      fp[i][0] = flux[0];
      fp[i][1] = flux[1];
      
      // double a = 0.5;
      // fm[i][0] = q0[i][1];
      // fm[i][1] = q0[i][1]*q0[i][1]/q0[i][0] + a*a*q0[i][0];
      // fp[i][0] = q0[i+1][1];
      // fp[i][1] = q0[i+1][1]*q0[i+1][1]/q0[i+1][0] + a*a*q0[i+1][0];
    }

    for(int i=1; i<nCell-1; i++) {
      fi[i] = laxFriedrichs1d_flux_numeric(fm[i-1], fp[i-1], dfm[i-1], dfp[i-1], qm[i-1], qp[i-1]);
      fo[i] = laxFriedrichs1d_flux_numeric(fm[i], fp[i], dfm[i], dfp[i], qm[i], qp[i]);
      //      std::cout << i << "\t" << fi[i] << std::endl;
    }

    for(int i=0; i<nCell; i++)
      q[i] = q0[i] + dt/dx*(fi[i] - fo[i]);

    boundaryCondition(q);

    q0 = q;
  }

  out.open("q.data");
  for(int i=0; i<nCell; i++)
    out << i*dx+dx/2 << "\t" << q0[i][0] << "\t" << q0[i][1] << std::endl;

  out.close();
}
