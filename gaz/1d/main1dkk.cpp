#include <iostream>
#include <cmath>
#include <fstream>
#include "Kokkos_Core.hpp"

#define KL KOKKOS_LAMBDA

namespace kk = Kokkos;

const int varDim = 2;

typedef kk::View<double*[varDim]> Vector;

struct Boundaries {
  kk::View<double*[varDim]> q;

  size_t nCell = q.extent(0);

  KOKKOS_INLINE_FUNCTION void operator()(int i, int j) const {
    q(0,0) = q(nCell-2,0);
    q(nCell-1,0) = q(1,0);

    q(0,1) = q(nCell-2,1);
    q(nCell-1,1) = q(1,1);
  }
};

KOKKOS_INLINE_FUNCTION double laxFriedrichs1d_flux_numeric(double const& fluxM, double const& fluxP, double dFluxM, double dFluxP, double const& valueM, double const& valueP)
{
  double f = 0.5*(fluxM + fluxP);

  double c = std::max(fabs(dFluxM), fabs(dFluxP));
  
  return f - 0.5*c*(valueP - valueM);
}


KOKKOS_INLINE_FUNCTION double dFlux(Vector const& q, int i, double a) {
  double rho = q(i,0);
  double rhoU = q(i,1);
  double u = rhoU/rho;
  
  return std::max(fabs(u+a), fabs(u-a));
  
}


// KOKKOS_INLINE_FUNCTION void boundaryCondition(Vector& q)
// {
//   size_t nCell = q.extent(0);
//   //kk::parallel_for("bounds", 1, [=] {
//   q(0,0) = q(nCell-2,0);
//   q(nCell-1,0) = q(1,0);

//   q(0,1) = q(nCell-2,1);
//   q(nCell-1,1) = q(1,1);
//   //});
// }

int main()
{
  kk::initialize();
  printf("Running in execution space %s, HostSpace %s\n",
      typeid(Kokkos::DefaultExecutionSpace).name(), typeid(Kokkos::DefaultHostExecutionSpace).name());
  std::cerr << "Simulation of a isothermal gaz starts ...\n";

  {

  enum Var {rho, mom};
  
  size_t nCell = 200;
  
  Vector q0("q0", nCell);
  auto hostq0 = kk::create_mirror_view(q0);

  Vector q("q", nCell);
  Vector qp("qp", nCell-1);
  Vector qm("qm", nCell-1);

  kk::View<double*> dfm("dfm", nCell-1);
  kk::View<double*> dfp("dfp", nCell-1);

  Vector fm("fm", nCell-1);
  Vector fp("fp", nCell-1);
  Vector fi("fi", nCell);
  Vector fo("fo", nCell);

  double lambda = 2*M_PI;
  double dx = lambda/(nCell - 2);
  double lx = lambda + dx;
  int nt = 1000;
  double cfl = 0.1;
  double dt = cfl*dx;
  double a = 0.5; // sound speed;

  std::cerr << "dt = " << dt << std::endl;

  auto full_range = kk::MDRangePolicy<kk::Rank<2>>({0,0},{q0.extent(0), q0.extent(1)});
  auto no_borders = kk::MDRangePolicy<kk::Rank<2>>({1,0},{q0.extent(0)-1, q0.extent(1)});

  kk::parallel_for("Initialisation", nCell, [=] (int i) {
    q0(i, rho) = 1;
    q0(i, mom) = sin(2*M_PI/lambda * (i+0.5)*dx);
  });

  kk::deep_copy(hostq0, q0);
  std::ofstream out;

  out.open("q0.data");
  for(int i=0; i<nCell; i++)
    out << i*dx+dx/2 << "\t" << hostq0(i,0) << "\t" << hostq0(i,1) << std::endl;

  out.close();

  for(int t=0; t<nt;t++) {

    if(t%10 == 0) {
      out.open("q-"+std::to_string(t)+".data");
      kk::deep_copy(hostq0, q0);
      for(int i=0; i<nCell; i++)
	out << i*dx+dx/2 << "\t" << hostq0(i,0) << "\t" << hostq0(i,1) << std::endl;
      
      out.close();
    }

    kk::parallel_for("Qs", nCell-1, [=] (int i){

      fm(i,0) = q0(i,1);
      fm(i,1) = q0(i,1)*q0(i,1)/q0(i,0) + a*a*q0(i,1);

      qm(i,0) = q0(i,0);
      qm(i,1) = q0(i,1);
      qp(i,0) = q0(i+1,0);
      qp(i,1) = q0(i+1,1);

      dfm(i) = dFlux(q0, i, a);
      dfp(i) = dFlux(q0, i+1, a);

      fp(i,0) = q0(i+1,1);
      fp(i,1) = q0(i+1,1)*q0(i+1,1)/q0(i+1,0) + a*a*q0(i+1,0);

    });
    
    // for(int i=0; i<nCell-1; i++) {
    //   fm[i][0] = q0[i][1];
    //   fm[i][1] = q0[i][1]*q0[i][1]/q0[i][0] + a*a*q0[i][0];

    //   qm[i][0] = q0[i][0];
    //   qm[i][1] = q0[i][1];
    //   qp[i][0] = q0[i+1][0];
    //   qp[i][1] = q0[i+1][1];

    //   dfm[i] = dFlux(q0[i], a);
    //   dfp[i] = dFlux(q0[i+1], a);
      
    //   fp[i][0] = q0[i+1][1];
    //   fp[i][1] = q0[i+1][1]*q0[i+1][1]/q0[i+1][0] + a*a*q0[i+1][0];
    // }

    kk::parallel_for("Flux", no_borders, [=] (int i, int var) {
      fi(i,var) = laxFriedrichs1d_flux_numeric(fm(i-1, var), fp(i-1, var), dfm(i-1), dfp(i-1), qm(i-1, var), qp(i-1, var));
      fo(i,var) = laxFriedrichs1d_flux_numeric(fm(i, var), fp(i, var), dfm(i), dfp(i), qm(i, var), qp(i, var));
    });

    // for(int i=1; i<nCell-1; i++) {
    //   fi[i] = laxFriedrichs1d_flux_numeric(fm[i-1], fp[i-1], dfm[i-1], dfp[i-1], qm[i-1], qp[i-1]);
    //   fo[i] = laxFriedrichs1d_flux_numeric(fm[i], fp[i], dfm[i], dfp[i], qm[i], qp[i]);
    // }
    
    kk::parallel_for("Q", full_range, [=] (int i, int var) {
      q(i,var) = q0(i,var)+dt/dx*(fi(i, var) - fo(i, var));
    });

    // for(int i=0; i<nCell; i++)
    //   q[i] = q0[i] + dt/dx*(fi[i] - fo[i]);

    Boundaries boundaries{q};

    kk::parallel_for("bounds", full_range, boundaries);

    kk::deep_copy(q0, q);
  }

  out.open("q.data");
  kk::deep_copy(hostq0,q0);
  for(int i=0; i<nCell; i++)
    out << i*dx+dx/2 << "\t" << hostq0(i,0) << "\t" << hostq0(i,1) << std::endl;

  out.close();
  }
  kk::finalize();
}
