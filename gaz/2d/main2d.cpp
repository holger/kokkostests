#include <iostream>
#include <fstream>

#include "../deps/BasisKlassen/trunk/array.hpp"
#include "../deps/BasisKlassen/trunk/vector.hpp"
#include "../deps/BasisKlassen/trunk/toString.hpp"
#include "../deps/BasisKlassen/trunk/multiArray.hpp"

typedef BK::MultiArray<2,BK::Array<double,3>> Matrix;

template<class Vector>
inline Vector laxFriedrichs1d_flux_numeric(Vector const& fluxM, Vector const& fluxP, double dFluxM, double dFluxP, Vector const& valueM, Vector const& valueP)
{
  Vector f = 0.5*(fluxM + fluxP);

  double c = std::max(fabs(dFluxM), fabs(dFluxP));
  
  return f - 0.5*c*(valueP - valueM);
}

double dFluxX(BK::Array<double,3> const& var, double a) {
  double rho = var[0];
  double rhoUx = var[1];
  double rhoUy = var[2];
  double ux = rhoUx/rho;
  double uy = rhoUy/rho;
  
  return std::max(fabs(ux+a), fabs(ux-a));
}

double dFluxY(BK::Array<double,3> const& var, double a) {
  double rho = var[0];
  double rhoUx = var[1];
  double rhoUy = var[2];
  double ux = rhoUx/rho;
  double uy = rhoUy/rho;
  
  return std::max(fabs(uy+a), fabs(uy-a));
}

void boundaryCondition(Matrix& var)
{
  size_t nCell = var.size(0);
  for(int j=0; j<nCell; j++) {
    var(0,j) = var(nCell-2,j);
    var(nCell-1,j) = var(1,j);    
  }

  for(int i=0; i<nCell; i++) {
    var(i,0) = var(i,nCell-2);
    var(i,nCell-1) = var(i,1);    
  }
}

int main()
{
  enum {rho, mx, my};
  
  int nCell = 40;
  
  double lambda = 2*M_PI;
  double dx = lambda/(nCell - 2);
  double dy = dx;
  double lx = lambda + dx;
  double ly = lx;

  int nt = 101;
  double cfl = 0.1;
  double dt = cfl*dx;
  double a = 0.5;

  std::cerr << "dt = " << dt << std::endl;
  
  Matrix var(nCell, nCell);
  Matrix var0(nCell, nCell);
  Matrix fmx(nCell, nCell);
  Matrix fmy(nCell, nCell);
  BK::MultiArray<2,double> dfmx(nCell, nCell);
  BK::MultiArray<2,double> dfmy(nCell, nCell);
  Matrix fpx(nCell, nCell);
  Matrix fpy(nCell, nCell);
  BK::MultiArray<2,double> dfpx(nCell, nCell);
  BK::MultiArray<2,double> dfpy(nCell, nCell);
  Matrix varm(nCell-1, nCell-1);
  Matrix varp(nCell-1, nCell-1);
  
  Matrix fmx_lf(nCell, nCell);
  Matrix fmy_lf(nCell, nCell);
  Matrix fpx_lf(nCell, nCell);
  Matrix fpy_lf(nCell, nCell);
    
  for(int i=0;i<nCell;i++)
    for(int j=0;j<nCell;j++) {
      var0(i, j)[rho] = 1;
      var0(i, j)[mx] = sin(2*M_PI/lambda * (i+0.5)*dx);
      var0(i, j)[my] = 0;
    }

  std::ofstream out;
  out.open("rho0.data");
  for(int i=0;i<nCell;i++) {
    for(int j=0;j<nCell;j++) 
      out << "\t" << var0(i,j)[rho];
    out << std::endl;
  }
  out.close();
  
  for(int t=0; t<nt;t++) {
    if(t%10 == 0) {

      out.open(BK::toString("rho-",t,".data"));
      for(int i=0;i<nCell;i++) {
	for(int j=0;j<nCell;j++) 
	  out << "\t" << var0(i,j)[rho];
	out << std::endl;
      }
      out.close();

      out.open(BK::toString("mx-",t,".data"));
      for(int i=0;i<nCell;i++) {
	for(int j=0;j<nCell;j++) 
	  out << "\t" << var0(i,j)[mx];
	out << std::endl;
      }
      out.close();

      out.open(BK::toString("my-",t,".data"));
      for(int i=0;i<nCell;i++) {
	for(int j=0;j<nCell;j++) 
	  out << "\t" << var0(i,j)[my];
	out << std::endl;
      }
      out.close();
    }

    for(int i=1; i<nCell-1; i++) 
      for(int j=1; j<nCell-1; j++) {
	fmx(i,j)[rho] = var0(i,j)[mx];
	fmx(i,j)[mx] = var0(i,j)[mx]*var0(i,j)[mx]/var0(i,j)[rho] + a*a*var0(i,j)[rho];
	fmx(i,j)[my] = var0(i,j)[mx]*var0(i,j)[my]/var0(i,j)[rho];

	fmy(i,j)[rho] = var0(i,j)[my];
	fmy(i,j)[mx] = var0(i,j)[my]*var0(i,j)[mx]/var0(i,j)[rho];
	fmy(i,j)[my] = var0(i,j)[my]*var0(i,j)[my]/var0(i,j)[rho] + a*a*var0(i,j)[rho];

	fpx(i,j)[rho] = var0(i+1,j)[mx];
	fpx(i,j)[mx] = var0(i+1,j)[mx]*var0(i+1,j)[mx]/var0(i+1,j)[rho] + a*a*var0(i+1,j)[rho];
	fpx(i,j)[my] = var0(i+1,j)[mx]*var0(i+1,j)[my]/var0(i+1,j)[rho];

	fpy(i,j)[rho] = var0(i,j+1)[my];
	fpy(i,j)[mx] = var0(i,j+1)[my]*var0(i,j+1)[mx]/var0(i,j+1)[rho];
	fpy(i,j)[my] = var0(i,j+1)[my]*var0(i,j+1)[my]/var0(i,j+1)[rho] + a*a*var0(i,j+1)[rho];

	dfmx(i,j) = dFluxX(var(i,j), a);
	dfpx(i,j) = dFluxX(var(i+1,j), a);
	dfmy(i,j) = dFluxY(var(i,j), a);
	dfpy(i,j) = dFluxY(var(i,j+1), a);
      }
    
    for(int i=1; i<nCell-1; i++) {
      for(int j=1; j<nCell-1; j++) {
	fmx_lf(i,j) = laxFriedrichs1d_flux_numeric(fmx(i-1,j), fpx(i-1,j), dfmx(i-1,j), dfpx(i-1,j), var0(i-1,j), var0(i,j));
	fpx_lf(i,j) = laxFriedrichs1d_flux_numeric(fmx(i,j), fpx(i,j), dfmx(i,j), dfpx(i,j), var0(i,j), var0(i+1,j));
	fmy_lf(i,j) = laxFriedrichs1d_flux_numeric(fmy(i,j-1), fpy(i,j-1), dfmy(i,j-1), dfpy(i,j-1), var0(i,j-1), var0(i,j));
	fpy_lf(i,j) = laxFriedrichs1d_flux_numeric(fmy(i,j), fpy(i,j), dfmy(i,j), dfpy(i,j), var0(i,j), var0(i,j+1));
	//       	std::cout << i << "\t" << j << "\t" << fmx_lf(i,j) << std::endl;
      }
    }

    for(int i=1; i<nCell-1; i++)
      for(int j=1; j<nCell-1; j++)
	var(i,j) = var0(i,j)+ dt/dx*(fmx(i,j) - fpx(i,j)) + dt/dy*(fmy(i,j) - fpy(i,j));
    
    boundaryCondition(var);

    var0 = var;
  }
}
